# Lab 06 : Infrastructure-as-code and configuration management - Terraform, Ansible and Gitlab

**Authors :** Cédric Rosat, Ian Escher

**Group :** GrR

**Creation date:** 28.05.2023

## Description

In this repository, we have the configurations files to :

- Create a cloud infrastructure on Google Cloud Platform
- Install a NGINX web server using Ansible

The objective is to apply the principles of Infrastructure-as-code and Desired State Configuration. 

This repository is a pedagogical demonstration for the Cloud course at HEIG-VD.